package main

import (
	"github.com/garyburd/redigo/redis"
	"strconv"
	"strings"

	"log"
	"os"
	"os/exec"
	"time"
)

var (
	ffmpeg   string = getPath("ffmpeg")
	openRTSP string = getPath("openRTSP")
	wc       string = getPath("wc")
	awk      string = getPath("awk")
	ps       string = getPath("ps")
	grep     string = getPath("grep")
	kill     string = getPath("kill")

	app_root string = "/usr/local/camera_dashboard"
	ch              = make(chan map[string]string)
)

func getPath(p string) string {
	path, err := exec.LookPath(p)
	checkError(err)
	return path
}

func checkError(err error) {
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
}

func getSnaphot(venue map[string]string) {
	dir := app_root + "/public/feeds/" + venue["venue_name"]
	os.MkdirAll(dir, 0755)

	feedCmd := openRTSP + ` -F ` + venue["venue_name"] + ` -d 10 -b 400000 ` + venue["cam_url"] + ` \
                                            && ` + ffmpeg + ` -y -i ` + venue["venue_name"] + `video-H264-1 -r 1 -vframes 1\
                                            -f image2 ` + app_root + `/public/feeds/` + venue["venue_name"] + `/` + venue["venue_name"] + `_big.jpeg\
                                            && ` + ffmpeg + ` -y -i ` + app_root + `/public/feeds/` + venue["venue_name"] + `/` + venue["venue_name"] + `_big.jpeg\
                                            -s 320x180 -f image2 ` + app_root + `/public/feeds/` + venue["venue_name"] + `/` + venue["venue_name"] + `.jpeg\
                                            && rm -f ` + venue["venue_name"] + `*`

	cmd := exec.Command("bash", "-c", feedCmd)
	// run command
	err := cmd.Run()
	if err == nil {
		ch <- venue
	}
}

// Kill any hanging openRTSP processes - https://jira.cet.uct.ac.za/browse/OPENCAST-613
func clearHangingProcesses() {
	countCmd := ps + ` aux | ` + grep + ` '[o]penRTSP' | ` + wc + ` -l`
	out, _ := exec.Command("bash", "-c", countCmd).Output()
	temp := string(out)
	temp = strings.TrimSpace(temp)
	stuckProcs, _ := strconv.Atoi(temp)

	if stuckProcs > 0 {
		clearCmd := kill + ` -9 $(` + ps + ` aux | ` + grep + ` '[o]penRTSP' | ` + awk + ` '{print $2}') > /var/log/feeds.log 2>&1`
		cmd := exec.Command("bash", "-c", clearCmd)
		_ = cmd.Run()
	}
}

func main() {
	c, err := redis.Dial("tcp", ":6379")
	defer c.Close()
	checkError(err)
	clearHangingProcesses()

	venue_list, _ := redis.Strings(c.Do("LRANGE", "venues", 0, -1))
	for _, v := range venue_list {
		venue := make(map[string]string)
		options := []string{"venue_name", "cam_url"}
		for _, o := range options {
			venue[o], _ = redis.String(c.Do("GET", "venue:"+v+":"+o))
		}
		venue["id"] = v
		go getSnaphot(venue)
	}
ForSelect:
	for {
		select {
		case venue := <-ch:
			// update the last_updated date
			image := app_root + "/public/feeds/" + venue["venue_name"] + "/" + venue["venue_name"] + ".jpeg"
			_, err := os.Open(image)
			// returns true if it gets "no such file or directory" error
			if !os.IsNotExist(err) {
				stats, err := os.Stat(image)
				checkError(err)
				c.Do("SET", "venue:"+venue["id"]+":last_updated", stats.ModTime())
			}
		case <-time.After(40000 * time.Millisecond):
			clearHangingProcesses()
			break ForSelect
		}
	}
}
