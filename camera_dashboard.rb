require 'sinatra'
require 'haml'
require 'redis'
require 'time'
require 'json'
require_relative 'matterhornconfig.rb'

include MatterhornConfig

set :haml, :format => :html5
REDIS = Redis.new

helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not authorized\n"
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? and @auth.basic? and @auth.credentials and
    @auth.credentials == MatterhornConfig::Dashboard::BASIC_AUTH
  end

  def get_venues
    venues = []
    venue_list = REDIS.lrange("venues", 0, -1)
    venue_list.each_with_index do |v_id, i|
      venues[i] = []
      venues[i] = get_venue(v_id, ["venue_name", "cam_url", "sync_time"])
      unless REDIS.get("venue:#{v_id}:last_updated").nil?
        venues[i] << Time.parse(REDIS.get("venue:#{v_id}:last_updated"))
      end
    end
    venues
  end

  def get_venue(v_id, options)
    venue = []
    options.each do |o|
      venue << REDIS.get("venue:#{v_id}:#{o}")
    end
    venue << v_id
    venue
  end

  def not_regularly_updating(last_updated)
    today = Time.now
    ((today - last_updated)/60).round > 10
  end

  def with_venue_list
    v = get_venues
    yield ( v ) if block_given?
  end

  def time_to_string(time)
    time.strftime("%Y-%m-%d %H:%M")
  end

  def total()
    get_venues.length
  end

  def online()
    total() - offline()
  end

  def offline()
    count = 0
    get_venues.each do |v|
      count = count + 1 if not_regularly_updating(v[4])
    end
    count
  end
end

get "/" do
  protected!
  with_venue_list {|v| haml :dashboard, :locals => {:venues => v} }
end

get "/tiled" do
  protected!
  with_venue_list {|v| haml :tiled, :locals => {:venues => v} }
end

get "/venues" do
  protected!
  with_venue_list {|v| haml :venues, :locals => {:venues => v} }
end

get "/ca.json" do
  content_type :json
  agents = []
  time_since_last_update = 0
  get_venues.each do |v|
    h = Hash.new
    h["ca_name"] = v[0]
    time_since_last_update = (Time.now - v[4]).to_i unless v[4].nil?
    h["time_since_last_update"] = time_since_last_update
    agents <<  h
  end
  { :agents => agents }.to_json
end
